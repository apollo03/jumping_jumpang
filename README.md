# README #

### What is this repository for? ###

Quick summary:
This repository is for the game developed in gdg-hackathon at IIT Kanpur during 10-11 October, 2015.

Version:
1.0

### How do I get set up? ###

Summary of set up:
Just fork this repository and install brackets IDE for editing and running this game.

Dependencies:
Phaser library, brackets software, server.

How to run tests:
Run files on server

How to play the game - jumping japang:
go to below link and enjoy the game:
http://home.iitk.ac.in/~amanna/

### Contribution guidelines ###

Code review:
You can edit the code as this is public repository.
You can send the suggestions about the game to the repository owner (Team: Apollo03).

### Who do I talk to? ###

Repo owner or admins: AM, PM, PK
 
Other community or team contact:
Apollo03